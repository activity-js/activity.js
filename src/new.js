import types from './types/index.js';

export default function ( $activity, options, load_state ) {
    var type = $activity.dataset.activity, cls;

    if ( ! type || ! ( type in types ) )
    {
        type = 'activity';
    }

    cls = types[ type ];

    return new cls( $activity, options, load_state );
};
