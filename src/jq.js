import aktivitet from "./index.js";
import $ from 'jquery';

// create jquery function
$.fn.activity = function ( options, data = {}, flags = 0 ) {
    // only return activity obj if this is a single el query
    if ( this.length == 1 )
    {
        if ( this[0].activity );
            return this[0].activity;
    }

    // init any activities that require it
    this.each( function () {
        if ( ! this.activity )
        {
            new aktivitet.types.activity( this, options, data, flags );
        }
    } );

    return this;
};

export default aktivitet;
