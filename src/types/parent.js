import selectors from "../selectors.js";
import delegate from "delegate-it";
import Activity from './activity.js';

import globals from '../globals.js';

selectors.PARENT = '[data-activity="parent"]';
selectors.PARENT_EXCLUDE = '.activity-parent-exclude';
selectors.PARENT_NOT_EXCLUDE = ':not( ' + selectors.PARENT_EXCLUDE + ')';

class Parent extends Activity
{
    auto_complete( $child )
    {
        if ( this.$activity.querySelectorAll( selectors.DIRECT_CHILDREN_NOT_COMPLETE + selectors.PARENT_NOT_EXCLUDE ).length == 0 )
            this.complete();
    }
}

Parent.on( 'completed', 'direct-children', function ( e ) {
    this.auto_complete( e.target );
} );

export default Parent;
