import events from "../events.js";
import selectors from "../selectors.js";
import delegate from "delegate-it";

import globals from '../globals.js';

class Activity
{
    constructor( $activity, options = {}, load_state = {} )
    {
        var $targets, _this = this;

        this.$activity = $activity;

        this.$activity.activity = this;

        // setup options
        // this.options = {} <- data-attrs <- construct-options
        this.options = Object.assign( {}, this.$activity.dataset, options );

        // ensure data-activity attr is set (makes it easier to identify activities)
        this.$activity.dataset.activity = this.options.activity;

        this.is_completed = false;
        this.is_locked = false;
        this.is_started = false;
        
        // load any initial data
        this.load( load_state );

        // ensure correct classes are set
        this.update_classes();
    }

    init() {}

    load( state )
    {
        if ( state.flags !== undefined )
        {
            if ( state.flags.is_completed !== undefined )
                this.is_completed = state.flags.is_completed
            if ( state.flags.is_locked !== undefined )
                this.is_locked = state.flags.is_locked
            if ( state.flags.is_started !== undefined )
                this.is_started = state.flags.is_started
        }
    }

    save()
    {
        return {
            flags: {
                is_completed: this.is_completed,
                is_locked: this.is_locked,
                is_started: this.is_started,
            }
        }
    }

    start()
    {
        this.is_started = true;
        this.update_classes();

        this.$activity.dispatchEvent( events.started() );
    }

    complete()
    {
        this.is_completed = true;
        this.update_classes();

        this.$activity.dispatchEvent( events.completed() );
    }

    unlock()
    {
        this.is_locked = false;
        this.update_classes();

        this.$activity.dispatchEvent( events.unlocked() );
    }

    update_classes()
    {
        this.$activity.classList.toggle( 'activity-started', this.is_started );
        this.$activity.classList.toggle( 'activity-completed', this.is_completed );
        this.$activity.classList.toggle( 'activity-locked', this.is_locked );
    }

    /**
     * generates query for require handlers (like data-require-complete etc)
     * handles getting previous or next activity or simply returns query for selecor
     */
    query( selector )
    {
        var $$activities,
            $this = this.$activity,
            $ = $this.querySelectorAll;

        switch ( selector )
        {
            case 'all':
                $this = $this.getRootNode();
                $ = $this.querySelectorAll;
                selector = selectors.ACTIVITY;
            break;
            case 'parent':
                selector = selectors.ACTIVITY_NOT_SCOPE;
                $ = this.$activity.closest;
            break;
            case 'next':
            case 'previous':
                $$activities = this.query( 'all' );
                if ( $$activities.length )
                    return [ $$activities[ Array.from( $$activities ).indexOf( this.$activity ) + ( selector == 'next' ? 1 : -1 ) ] ];
                else
                    return;
            break;
            case 'next-sibling':
            case 'previous-sibling':
                $$activities = this.query( 'parent' ).activity.query( 'direct-children' );
                if ( $$activities.length )
                    return [ $$activities[ Array.from( $$activities ).indexOf( this.$activity ) + ( selector == 'next-sibling' ? 1 : -1 ) ] ];
                else
                    return;
            break;
            case 'children':
                selector = selectors.ACTIVITY;
            break;
            case 'direct-children':
                selector = selectors.DIRECT_CHILD;
            break;
        }

        return $.call( $this, selector );
    }

    on( event, selector, callback )
    {
        var $target;

        switch ( selector )
        {
            case 'next':
            case 'previous':
                $target = this.query( selector )[0];
                selector = null;
            break;
            case 'children':
                $target = this.$activity;
                selector = selectors.ACTIVITY;
            break;
            case 'direct-children':
                $target = this.$activity;
                selector = selectors.DIRECT_CHILD;
            break;
            default:
                $target = globals.$event_root;

        }
        if ( selector )
            return delegate( $target, selector, event, callback );
        else
            return $target.addEventListener( event, callback );
    }

    static is( $el )
    {
        return $el.activity && $el.activity instanceof this;
    }

    static on( event, selector, callback )
    {
        var cls = this;

        switch ( selector )
        {
            case 'self':
                globals.$event_root.addEventListener( event, function ( e ) {
                    if ( cls.is( e.target ) )
                        callback.call( e.target.activity, e );
                } );
            break;
            case 'children':
                globals.$event_root.addEventListener( event, function ( e ) {
                    var $parent = e.target.closest( selectors.ACTIVITY );
                    if ( $parent && cls.is( $parent ) )
                        callback.call( $parent.activity, e );
                } );
            break;
            case 'direct-children':
                globals.$event_root.addEventListener( event, function ( e ) {
                    var $parent = e.target.closest( selectors.ACTIVITY_NOT_SCOPE );
                    if ( $parent && cls.is( $parent ) )
                        callback.call( $parent.activity, e );
                } );
            break;
        }
    }
}

globals.$event_root.addEventListener( 'init', function ( e ) {
    document.querySelectorAll( selectors.ACTIVITY )
        .forEach( function ( $activity, i ) {
            $activity.activity.init();
        } );
} );

export default Activity;