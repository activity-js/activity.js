// where delegated event handlers are attached
const $ = document.querySelector.bind( document );
const $$ = document.querySelectorAll.bind( document );
const $event_root = document;

const globals = {
    $, $$,
    $event_root
};

export default globals;